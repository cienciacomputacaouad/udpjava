package com.velha.uniandrade;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.Scanner;

public class GameServer {
	
	public static void main(String args[])
	{
		// Aloca a mem�ria do tabuleiro
		StringBuilder tabuleiro = new StringBuilder("         ");
		
		Scanner keyboard = new Scanner(System.in);
		
		DatagramSocket socket = null;
		
		try
		{
			socket = new DatagramSocket(6789);
			byte[] buffer = new byte[1024];
			System.out.println("Server pronto");
			
			DatagramPacket request = new DatagramPacket(buffer, buffer.length);
			
			socket.receive(request);
			System.out.println("Cliente conectado. Vez do server...");
			
			while (true)
			{
				
				
				System.out.println(tabuleiro.charAt(0) + "|" + tabuleiro.charAt(1) + "|" + tabuleiro.charAt(2));
				System.out.println("------");
				System.out.println(tabuleiro.charAt(3) + "|" + tabuleiro.charAt(4) + "|" + tabuleiro.charAt(5));
				System.out.println("------");
				System.out.println(tabuleiro.charAt(6) + "|" + tabuleiro.charAt(7) + "|" + tabuleiro.charAt(8));
				
				System.out.println("Posicao da jogada: ");
				int pos = keyboard.nextInt();
				
				
				tabuleiro.setCharAt(pos, 'O');
				
				System.out.print(tabuleiro);
				
				buffer = tabuleiro.toString().getBytes();
				
				DatagramPacket reply = new DatagramPacket(buffer,
						buffer.length,
						request.getAddress(),
						request.getPort());
				
				socket.send(reply);
				
				System.out.println("Esperando o cliente");
				
				socket.receive(request);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
