package com.velha.uniandrade;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class GameClient {
	
	public static void main(String args[])
	{
		DatagramSocket socket = null;
		
		try {
			while (true)
			{
			String msg = "0123456789";
			socket = new DatagramSocket();
			
			byte[] m = msg.getBytes();
			InetAddress host = InetAddress.getByName("127.0.0.1");
			int serverport = 6789;
			DatagramPacket request = new DatagramPacket(m,
					m.length,
					host,
					serverport);
			
			socket.send(request);
			byte[] buffer = new byte[1024];
			DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
			
			socket.receive(reply);
			System.out.println("Reply: " + new String(reply.getData()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			if( socket != null)
				socket.close();
		}
		
	}

}
